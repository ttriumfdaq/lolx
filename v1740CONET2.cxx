/*****************************************************************************/
/**
   \file v1740CONET2.cxx

   ## Contents

   This file contains the class implementation for the v1740 module driver.
*****************************************************************************/

#include "v1740CONET2.hxx"

#define UNUSED(x) ((void)(x)) //!< Suppress compiler warnings

//! Configuration string for this module. (ODB: /Equipment/[eq_name]/Settings/[board_name]/)
const char * v1740CONET2::config_str[] = {\
  "setup = INT : 0",\
  "Acq mode = INT : 1",\
  "Group Configuration = DWORD : 0",\
  "Buffer organization = INT : 8",\
  "Custom size = INT : 0",\
  "Group Mask = DWORD : 0xFF",\
  "Trigger Source = DWORD : 0x40000000",\
  "Trigger Output = DWORD : 0x40000000",\
  "Post Trigger = DWORD : 200",\
  "Almost Full level = DWORD : 512",\
  "Threshold = DWORD[8] :",\
  "[0] 1900",\
  "[1] 1900",\
  "[2] 2080",\
  "[3] 9",\
  "[4] 9",\
  "[5] 9",\
  "[6] 9",\
  "[7] 9",\
  "DAC = DWORD[8] :",\
  "[0] 35000",\
  "[1] 35000",\
  "[2] 35000",\
  "[3] 35000",\
  "[4] 35000",\
  "[5] 35000",\
  "[6] 9190",\
  "[7] 9190",\
  "Enable Filtering = BOOL : y",\
  "Filtering Threshold = INT : 3750",\
  NULL
};

/**
 * \brief   Constructor for the module object
 *
 * Set the basic hardware parameters
 *
 * \param   [in]  link      Optical link number
 * \param   [in]  board     Board number on the optical link
 * \param   [in]  moduleID  Unique ID assigned to module
 */
v1740CONET2::v1740CONET2(int link, int board, int moduleID, HNDLE hDB) :
link_(link), board_(board), moduleID_(moduleID), odb_handle_(hDB), num_events_in_rb_(0)
{
  device_handle_ = -1;
  settings_handle_ = 0;
  settings_loaded_=false;
  running_ = false;
  rb_handle_ = -1;
  verbosity_ = 0;
}
/**
 * Move constructor needed because we're putting v1740CONET2 objects in a vector which requires
 * either the copy or move operation.  The implicit move constructor (or copy constructor)
 * cannot be created by the compiler because our class contains an atomic object with a
 * deleted copy constructor. */
v1740CONET2::v1740CONET2(v1740CONET2&& other) noexcept
: link_(std::move(other.link_)), board_(std::move(other.board_)),
  moduleID_(std::move(other.moduleID_)), odb_handle_(std::move(other.odb_handle_)),
  num_events_in_rb_(other.num_events_in_rb_.load())
{
  device_handle_ = std::move(other.device_handle_);
  settings_handle_ = std::move(other.settings_handle_);
  settings_loaded_ = std::move(other.settings_loaded_);
  running_= std::move(other.running_);
  rb_handle_ = std::move(other.rb_handle_);
  verbosity_ = std::move(other.verbosity_);
  config = std::move(other.config);
}
v1740CONET2& v1740CONET2::operator=(v1740CONET2&& other) noexcept
{
  if (this != &other){  //if trying to assign object to itself

    link_ = std::move(other.link_);
    board_ = std::move(other.board_);
    moduleID_ = std::move(other.moduleID_);
    odb_handle_ = std::move(other.odb_handle_);
    num_events_in_rb_ = other.num_events_in_rb_.load();
    device_handle_ = std::move(other.device_handle_);
    settings_handle_ = std::move(other.settings_handle_);
    settings_loaded_ = std::move(other.settings_loaded_);
    running_= std::move(other.running_);
    rb_handle_ = std::move(other.rb_handle_);
    verbosity_ = std::move(other.verbosity_);
    config = std::move(other.config);
  }

  return *this;

}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Destructor for the module object
 *
 * Nothing to do.
 */
  v1740CONET2::~v1740CONET2() {}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Get short string identifying the module's board number
 *
 * \return  name string
 */
std::string v1740CONET2::GetName()
{
  std::stringstream txt;
  txt << "B" << board_;
  return txt.str();
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Get connected status
 *
 * \return  true if board is connected
 */
bool v1740CONET2::IsConnected()
{
  return (device_handle_ >= 0);
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Get run status
 *
 * \return  true if run is started
 */
bool v1740CONET2::IsRunning()
{
  return running_;
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Connect the board through the optical link
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1740CONET2::Connect()
{
  if (verbosity_) std::cout << GetName() << "::Connect()\n";

  if (IsConnected()) {
    cm_msg(MERROR,"Connect","Board %d already connected\n", this->GetModuleID());
    return false;
  }

  if (verbosity_) std::cout << "Opening device (l,b) = (" << link_ << "," << board_ << ")" << std::endl;

  CAENComm_ErrorCode sCAEN;

#if SIMULATION
  sCAEN = CAENComm_Success;
  _device_handle = 1;  //random
#else
  sCAEN = CAENComm_OpenDevice(CAENComm_PCIE_OpticalLink, link_, board_, 0, &device_handle_);
#endif

  if (sCAEN == CAENComm_Success) {
#if SIMULATION
    printf("Connected successfully to module; handle: %d\n",this->GetHandle());
#else
    printf("Link#:%d Board#:%d Module_Handle[%d]:%d\n",
           link_, board_, moduleID_, this->GetSettingsHandle());
#endif
  }
  else {
    device_handle_ = -1;
    cm_msg(MERROR, "Connect", "Link#:%d iBoard#:%d error:%d", link_, board_, sCAEN);
    return false;
  }

  return true;
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Disconnect the board through the optical link
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1740CONET2::Disconnect()
{
  if (verbosity_) std::cout << GetName() << "::Disconnect()\n";

  if (!IsConnected()) {
    cm_msg(MERROR,"Disconnect","Board %d already disconnected\n", this->GetModuleID());
    return false;
  }
  if (IsRunning()) {
    cm_msg(MERROR,"Disconnect","Can't disconnect board %d: run in progress\n", this->GetModuleID());
    return false;
  }

  if (verbosity_) std::cout << "Closing device (l,b) = (" << link_ << "," << board_ << ")" << std::endl;

  CAENComm_ErrorCode sCAEN;

#if SIMULATION  //Simulate success closing device
  sCAEN = CAENComm_Success;
#else
  sCAEN = CAENComm_CloseDevice(device_handle_);
#endif

  if(sCAEN == CAENComm_Success){
    device_handle_ = -1;
  }
  else
    return false;

  return true;
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Start data acquisition
 *
 * Do a full reset of the board and reset all the critical registers.
 * Write to Acquisition Control reg to put board in RUN mode. 
 * Set running_ flag true.
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1740CONET2::StartRun()
{

  if (verbosity_) std::cout << GetName() << "::StartRun()\n";

  if (IsRunning()) {
    cm_msg(MERROR,"StartRun","Board %d already started\n", this->GetModuleID());
    return false;
  }
  if (!IsConnected()) {
    cm_msg(MERROR,"StartRun","Board %d disconnected\n", this->GetModuleID());
    return false;
  }

  //Re-read the record from ODB, it may have changed
  int size = sizeof(V1740_CONFIG_SETTINGS);
  db_get_record(odb_handle_, settings_handle_, &config, &size, 0);

  CAENComm_ErrorCode sCAEN;
  uint32_t temp;
  
  //use preset setting if enabled
  if (config.setup != 0) {
    SetupPreset_(config.setup);
    //else use odb values
  } else {

    AcqCtl_(config.acq_mode);

    // Include by default the Memory Sequential Access and Trigger Overlap Enable (0x12)
    WriteReg_(V1740_GROUP_CONFIG        , 0x12 | config.group_config);
    WriteReg_(V1740_BUFFER_ORGANIZATION , config.buffer_organization);
    WriteReg_(V1740_CUSTOM_SIZE         , config.custom_size);
    WriteReg_(V1740_GROUP_EN_MASK       , config.group_mask);
    AcqCtl_(V1740_COUNT_ACCEPTED_TRIGGER);
    WriteReg_(V1740_TRIG_SRCE_EN_MASK     , config.trigger_source);
    WriteReg_(V1740_FP_TRIGGER_OUT_EN_MASK, config.trigger_output);
    WriteReg_(V1740_POST_TRIGGER_SETTING  , config.post_trigger);
    WriteReg_(V1740_ALMOST_FULL_LEVEL,      config.almost_full);
    ReadReg_(V1740_GROUP_CONFIG, &temp);
    WriteReg_(V1740_MONITOR_MODE, 0x3);  // buffer occupancy
    WriteReg_(V1740_BLT_EVENT_NB, 0x1);  // # of event per BLT



    // Setup Busy daisy chaining
    sCAEN = WriteReg_(V1740_FP_IO_CONTROL,  0x104); // 0x100:enable new config, 0x4:LVDS I/O[3..0] output
    sCAEN = WriteReg_(V1740_FP_LVDS_IO_CRTL,0x022); // 0x20: I/O[7..4] input mode nBusy/Veto (4:nBusy input)
                                                    // 0x02: I/O[3..0] output mode nBusy/Veto (1:Busy)

    /*sCAEN = ov1740_GroupConfig(GetHandle()
      , config.trigger_positive_pulse == 1 ? V1740_TRIGGER_OVERTH : V1740_TRIGGER_UNDERTH);*/
    WriteReg_(V1740_VME_CONTROL, V1740_ALIGN64);

    // Set individual channel stuff
    for (int i=0;i<8;i++) {
      //group threshold
      sCAEN = ov1740_GroupSet(GetDeviceHandle(), i, V1740_GROUP_THRESHOLD, config.threshold[i]);
      sCAEN = ov1740_GroupGet(GetDeviceHandle(), i, V1740_GROUP_THRESHOLD, &temp);

      //set DAC
      //Use my own to avoid printing "reg: dac: " line
      //sCAEN = ov1740_GroupDACSet(GetDeviceHandle(), i, config.dac[i]);
      sCAEN = GroupDACSet(GetDeviceHandle(), i, config.dac[i]);

      //disable channel triggers
      WriteReg_(V1740_GROUP_CH_TRG_MASK + (i<<8), 0);
    }

    // Doesn't set the ID in the data stream!
    WriteReg_( V1740_BOARD_ID, 6);
    //    ov1740_Status(GetDeviceHandle());
  }

  UNUSED(sCAEN);

  // Check finally for Acquisition status
  DWORD reg;
  sCAEN = ReadReg_(V1740_ACQUISITION_STATUS, &reg);
  if ((reg & 0xF0) != 0x80) {
    cm_msg(MERROR, "StartRun", "Board %d not initilized properly acq status:0x%x",  moduleID_, reg);
    return false;
  }

  CAENComm_ErrorCode e = AcqCtl_(V1740_RUN_START);
  if (e == CAENComm_Success)
    running_=true;
  else
    return false;

  return true;
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Start data acquisition
 *
 * Write to Acquisition Control reg to put board in STOP mode.
 * Set running_ flag false.
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1740CONET2::StopRun()
{
  if (verbosity_) std::cout << GetName() << "::StopRun()\n";

  if (!IsRunning()) {
    cm_msg(MERROR,"StopRun","Board %d already stopped\n", this->GetModuleID());
    return false;
  }
  if (!IsConnected()) {
    cm_msg(MERROR,"StopRun","Board %d disconnected\n", this->GetModuleID());
    return false;
  }

  CAENComm_ErrorCode e = AcqCtl_(V1740_RUN_STOP);
  if (e == CAENComm_Success)
    running_=false;
  else
    return false;

  return true;
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Setup board registers using preset (see ov1740.c:ov1740_Setup())
 *
 * Setup board registers using a preset defined in the midas file ov1740.c
 * - Mode 0x0: "Setup Skip\n"
 * - Mode 0x1: "Trigger from FP, 8ch, 1Ks, postTrigger 800\n"
 * - Mode 0x2: "Trigger from LEMO\n"
 *
 * \param   [in]  mode Configuration mode number
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1740CONET2::SetupPreset_(int mode)
{
#if SIMULATION
  return CAENComm_Success;
#else
  return ov1740_Setup(device_handle_, mode);
#endif
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Control data acquisition
 *
 * Write to Acquisition Control reg
 *
 * \param   [in]  operation acquisition mode (see v1740.h)
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1740CONET2::AcqCtl_(uint32_t operation)
{
#if SIMULATION
  return CAENComm_Success;
#else
  //  return ov1740_AcqCtl(device_handle_, operation);
  uint32_t reg;
  CAENComm_ErrorCode sCAEN;

  sCAEN = CAENComm_Read32(device_handle_, V1740_ACQUISITION_CONTROL, &reg);

  switch (operation) {
  case V1740_RUN_START:
    //        printf("Reg: 0x%x\n", V1740_ACQUISITION_CONTROL);
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, (reg | 0x4));
    break;
  case V1740_RUN_STOP:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, (reg & ~( 0x4)));

    break;
  case V1740_REGISTER_RUN_MODE:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, 0x100);
    break;
  case V1740_SIN_RUN_MODE:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, 0x101);
    break;
  case V1740_SIN_GATE_RUN_MODE:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, 0x102);
    break;
  case V1740_MULTI_BOARD_SYNC_MODE:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, 0x103);
    break;
  case V1740_COUNT_ACCEPTED_TRIGGER:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, (reg & ~( 0x8)));
    break;
  case V1740_COUNT_ALL_TRIGGER:
    sCAEN = CAENComm_Write32(device_handle_, V1740_ACQUISITION_CONTROL, (reg | 0x8));
    break;
  default:
    printf("operation %d not defined\n", operation);
    break;
  }
  return sCAEN;

#endif
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Read 32-bit register
 *
 * \param   [in]  address  address of the register to read
 * \param   [out] val      value read from register
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1740CONET2::ReadReg_(DWORD address, DWORD *val)
{
  if (verbosity_ >= 2) std::cout << GetName() << "::ReadReg_(" << std::hex << address << ")" << std::endl;
#if SIMULATION
  return CAENComm_Success;
#else
  return CAENComm_Read32(device_handle_, address, val);
#endif
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Write to 32-bit register
 *
 * \param   [in]  address  address of the register to write to
 * \param   [in]  val      value to write to the register
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1740CONET2::WriteReg_(DWORD address, DWORD val)
{
  if (verbosity_ >= 2) std::cout << GetName() << "::WriteReg(" << std::hex << address << "," << val << ")" << std::endl;
#if SIMULATION
  return CAENComm_Success;
#else
  return CAENComm_Write32(device_handle_, address, val);
#endif
}

//
//--------------------------------------------------------------------------------
bool v1740CONET2::ReadReg(DWORD address, DWORD *val)
{
  return (ReadReg_(address, val) == CAENComm_Success);
}

//
//--------------------------------------------------------------------------------
bool v1740CONET2::WriteReg(DWORD address, DWORD val)
{
  return (WriteReg(address, val) == CAENComm_Success);
}

//
//--------------------------------------------------------------------------------
/**
 * \brief   Poll Event Stored register
 *
 * Check Event Stored register for any event stored
 *
 * \param   [out]  val     Number of events stored
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1740CONET2::Poll(DWORD *val)
{
#if SIMULATION
  return true;
#else
  CAENComm_ErrorCode sCAEN = CAENComm_Read32(device_handle_, V1740_EVENT_STORED, val);
  return (sCAEN == CAENComm_Success);
#endif
}


//
//--------------------------------------------------------------------------------
//! Maximum size of data to read using BLT (32-bit) cycle
#define MAX_BLT_READ_SIZE_BYTES 10000
bool v1740CONET2::CheckEvent() {
	   DWORD vmeStat, eStored;
	      this->ReadReg(V1740_VME_STATUS, &vmeStat);
		     return (vmeStat & 0x1);

  return false;
}

//
//--------------------------------------------------------------------------------
bool v1740CONET2::ReadEvent(void *wp) {
  CAENComm_ErrorCode sCAEN;
  DWORD size_remaining_dwords, to_read_dwords, *pdata = (DWORD *)wp;
  int dwords_read_total = 0, dwords_read = 0;
  
  sCAEN = ReadReg_(V1740_EVENT_SIZE, &size_remaining_dwords);

  while ((size_remaining_dwords > 0) && (sCAEN == CAENComm_Success)) {      
    //calculate amount of data to be read in this iteration
    to_read_dwords = size_remaining_dwords > MAX_BLT_READ_SIZE_BYTES/4 ? MAX_BLT_READ_SIZE_BYTES/4 : size_remaining_dwords;
    sCAEN = CAENComm_BLTRead(device_handle_, V1740_EVENT_READOUT_BUFFER, (DWORD *)pdata, to_read_dwords, &dwords_read);
    if (verbosity_ >= 2) std::cout << sCAEN << " = BLTRead(handle=" << device_handle_
                                   << ", addr=" << V1740_EVENT_READOUT_BUFFER
                                   << ", pdata=" << pdata
                                   << ", to_read_dwords=" << to_read_dwords
                                   << ", dwords_read returned " << dwords_read << ");" << std::endl;
    //increment pointers/counters
    dwords_read_total += dwords_read;
    size_remaining_dwords -= dwords_read;
    pdata += dwords_read;
  }
  rb_increment_wp(this->GetRingBufferHandle(), dwords_read_total*4);
  this->IncrementNumEventsInRB(); //atomic
  return (sCAEN == CAENComm_Success);
}

//
//---------------------------------------------------------------------------------------------------------
bool v1740CONET2::ReadEventCAEN(void *wp) {
  CAENComm_ErrorCode sCAEN;
  
  /*******************************************************************************************
   **** WARNING: The CAEN doc says that the 4th parameter of CAENComm_BLTRead (BltSize) is
   **** in bytes.  That is FALSE.  It must be in 32-bit dwords.  P-L
   *******************************************************************************************/
  
  DWORD to_read_dwords=1000000, *pdata = (DWORD *)wp;
  int dwords_read = 0;
  
  sCAEN = CAENComm_BLTRead(device_handle_, V1740_EVENT_READOUT_BUFFER, (DWORD *)pdata, to_read_dwords, &dwords_read);
	
	if (verbosity_ >= 2) std::cout << sCAEN << " = BLTRead(handle=" << device_handle_
												 //std::cout << sCAEN << " = BLTRead(handle=" << device_handle_
                                 << ", addr=" << V1740_EVENT_READOUT_BUFFER
                                 << ", pdata=" << pdata
                                 << ", to_read_dwords=" << to_read_dwords
                                 << ", dwords_read returned " << dwords_read << ");" << std::endl;
  
  // if dwords_read == to_read_dwords, BLT tranfert may have been limited and all the event not transfered...
  if( dwords_read == (int)to_read_dwords)
    cm_msg(MERROR,"ReadEvent", "BLT transfert of maximum size (%d/%d)", dwords_read, to_read_dwords);
  
  // If no event ready, dwords_read will stay at 0, return
  // Since the event is always smaller than 1000000, BLTRead returns CAENComm_Terminated, not CAENComm_success
  if (dwords_read != 0) {
    rb_increment_wp(this->GetRingBufferHandle(), dwords_read*sizeof(int));
    this->IncrementNumEventsInRB(); //atomic
  }
  
  bool result = (sCAEN == CAENComm_Terminated);
  if (result == false) 
    cm_msg(MERROR,"ReadEvent", "V1740 BLT Read return error: %d", sCAEN);
  return result;
}

//
//---------------------------------------------------------------------------------------------------------
bool v1740CONET2::FillEventBank(char * pevent) {
  
  //  printf(" in FillEventBank\n");
  
  if (! this->IsConnected()) {
    cm_msg(MERROR,"FillEventBank","Board %d disconnected\n", this->GetModuleID());
    return false;
  }
  
  DWORD *src;
  DWORD *dest;
  
  int status = rb_get_rp(this->GetRingBufferHandle(), (void**)&src, 200);
  if (status == DB_TIMEOUT) {
    cm_msg(MERROR,"FillEventBank", "Got rp timeout for module %d\n", this->GetModuleID());
    return 0;
  }

#ifdef DEBUGTHREAD
  printf("Event size (words): %u\n", *src & 0x0FFFFFFF);
  printf("Event Counter: %u\n", *(src+2) & 0x00FFFFFF);
  printf("TTT: %u\n", *(src + 3));
#endif

  assert((*src & 0xF0000000) == 0xA0000000);  //Event header indicator

  uint32_t size_words = *src & 0x0FFFFFFF;
  int dest_size_words = size_words;

  // >>> create data bank
  char bankName[5];
  sprintf(bankName,"W4%02d", this->GetModuleID());
  bk_create(pevent, bankName, TID_DWORD, (void **)&dest);

  if (!config.enable_filtering) {
    // Originally behaviour - just copy over the V1740 information directly to
    // the midas banks.
    memcpy(dest, src, size_words*4);
  } else {
    // New behaviour - only copy over channels with large pulses.
    dest_size_words = WriteFilteredWaveforms(src, dest);
  }

  //  printf("NumEvent:%d ", this->GetNumEventsInRB());
  this->DecrementNumEventsInRB(); //atomic
  //  printf("NumEvent:%d\n", this->GetNumEventsInRB());
  rb_increment_rp(this->GetRingBufferHandle(), size_words*4);

  bk_close(pevent, dest + dest_size_words);

  return true;

}

//
//---------------------------------------------------------------------------------------------------------
int v1740CONET2::WriteFilteredWaveforms(DWORD* src_pdata, DWORD* dest_pdata) {

  // Filter V1740 information based on results.
  DWORD* dest_size = dest_pdata;
  *dest_pdata++ = src_pdata[0]; // Lower 24 bits are midas data size (we'll set this later)
  DWORD* dest_mask = dest_pdata;
  *dest_pdata++ = src_pdata[1]; // Midas channel group mask
  *dest_pdata++ = src_pdata[2]; // Midas event counter
  *dest_pdata++ = src_pdata[3]; // Midas trigger tag

  int src_size = (src_pdata[0] & 0xFFFFFF);
  int src_group_mask = (src_pdata[1] & 0xFF);
  int dest_words = 4;
  int dest_group_mask = 0;
  int src_word = 4;

  // count the number of active groups
  int src_group_count = 0;
  for (int32_t grp=0; grp<8; ++grp) {
      if (src_group_mask & (1 << grp)) ++src_group_count;
  }

  if (!src_group_count) {
    // No active groups - don't write anything.
    return dest_words;
  }

  // each chunk of 9 words contains 24 12-bit samples
  // (3 samples for each of the 8 channels)
  int nChunks = (src_size - 4) / (9 * src_group_count);   // number of chunks per group
  if (!nChunks) {
    return dest_words;
  }

  for (int grp=0; grp<8; ++grp) {
      // consider only active groups
      if (!(src_group_mask & (1 << grp))) {
        continue;
      }

      int start_src_word = src_word;
      int end_src_word = src_word + nChunks * 9;

      // We need to loop through the data to see what the minimum
      // sample value is.
      bool should_keep_grp = false;
      unsigned int num_samples = 0;
      unsigned int shift = 0;

      while (src_word < end_src_word) {
        // We have 2.5 pack data, so it's a bit of a pain to disentangle.
        // 8 12-bit samples packed into 3 32-bit words,
        // 3 samples per channel then step to the next channel
        unsigned int val = (src_pdata[src_word] >> shift) & 0xFFF;

        if ((shift += 12) >= 32) {
            src_word++;  // step to next data word

            if ((shift -= 32) > 0) {
                // Any remaining high bits for this sample come from the
                // low bits of the next data word
                val |= (src_pdata[src_word] << (12 - shift)) & 0xFFF;
            }
        }

        int channel = (num_samples++ / 3) & 0x07;

        if (val < config.filtering_threshold) {
          should_keep_grp = true;
          break;
        }

        // TODO: Later we can consider filtering based on channel rather than
        // group, and doing our own packing.
        (void)channel;
      }

      // Now reset to the start of the group, in case we need to
      // copy it over. (Also so that we're in a well-defined position
      // again, in case we broke out of the loop.)
      src_word = start_src_word;

      if (should_keep_grp) {
        // Copy the data for this group.
        dest_group_mask |= (1 << grp);
        while (src_word < end_src_word) {
          *dest_pdata++ = src_pdata[src_word++];
          dest_words++;
        }
      } else {
        // Skip ahead to the next group.
        src_word = end_src_word;
      }
  }

  // Update the mask. Only want to update the lower byte of it,
  // leaving the rest in-tact. First clear the lower byte, then
  // set it.
  *dest_mask &= ~0xFF;
  *dest_mask |= (dest_group_mask & 0xFF);

  // Similarly for the size in words - leave the upper byte alone.
  *dest_size &= ~0xFFFFFF;
  *dest_size |= (dest_words & 0xFFFFFF);

  return dest_words;
}


//
//---------------------------------------------------------------------------------------------------------
bool v1740CONET2::FillBufferLevelBank(char * pevent) {
  
  if (! this->IsConnected()) {
    cm_msg(MERROR,"FillBufferLevelBank","Board %d disconnected\n", this->GetModuleID());
    return false;
  }
  
  DWORD *pdata, eStored, almostFull;
  int rb_level;
  char statBankName[5];
  CAENComm_ErrorCode sCAEN;

  sprintf(statBankName, "B4%02d", this->GetModuleID());
  bk_create(pevent, statBankName, TID_DWORD, (void **)&pdata);

  //Get v1740 buffer level
  sCAEN = ReadReg_(V1740_EVENT_STORED, &eStored);
  sCAEN = ReadReg_(V1740_ALMOST_FULL_LEVEL, &almostFull);
  //Get ring buffer level
  rb_get_buffer_level(this->GetRingBufferHandle(), &rb_level);

  *pdata++ = eStored;
  /***
   * Note: There is no register in the v1740 indicating a busy
   * signal being output.  So we have to deduce it from the buffer
   * level and the almost full setting
   */
  int busy = 0;
  if(almostFull == 0) {
    /* If the almost full register is set to 0,
     * the busy signal comes out only when all the
     * 1024 buffers are used */
    busy = (eStored == 1024) ? 1 : 0;
  }
  else {
    busy = (eStored >= almostFull) ? 1 : 0;
  }
  *pdata++ = busy*500; //Make it 500 for better histogram display

  *pdata++ = rb_level;

  if(busy)
    printf(" %d(B)/%u ", eStored, rb_level);
  else
    printf(" %d/%u", eStored, rb_level);

  bk_close(pevent, pdata);

  return (sCAEN == CAENComm_Success);
}

//
//---------------------------------------------------------------------------------------------------------
/**
 * \brief   Send a software trigger to the board
 *
 * Send a software trigger to the board.  May require
 * software triggers to be enabled in register.
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1740CONET2::SendTrigger() {
  if (verbosity_) std::cout << GetName() << "::SendTrigger()" << std::endl;
  if (!IsConnected()) {
    cm_msg(MERROR,"SendTrigger","Board %d disconnected\n", this->GetModuleID());
    return false;
  }
  
  if (verbosity_) std::cout << "Sending Trigger (l,b) = (" << link_ << "," << board_ << ")" << std::endl;
  return (WriteReg(V1740_SOFTWARE_TRIGGER, 0x1) == CAENComm_Success);
}

//
//---------------------------------------------------------------------------------------------------------
/**
 * \brief   Set the ODB record for this module
 *
 * Create a record for the board with settings from the configuration
 * string (v1740CONET2::config_str) if it doesn't exist or merge with
 * existing record. Create hotlink with callback function for when the
 * record is updated.  Get the handle to the record.
 *
 * Ex: For a frontend with index number 2 and board number 0, this
 * record will be created/merged:
 *
 * /Equipment/FEV1740I2/Settings/Board0
 *
 * \param   [in]  h        main ODB handle
 * \param   [in]  cb_func  Callback function to call when record is updated
 * \return  ODB Error Code (see midas.h)
 */
int v1740CONET2::SetBoardRecord(HNDLE h, void(*cb_func)(INT,INT,void*)) {
  char set_str[80];
  int status,size;

#if SIMULATION
  sprintf(set_str, "/Equipment/FEV1740MT_SIM/Settings/Board%d", board_);
#else
  sprintf(set_str, "/Equipment/FEV1740MT/Settings/Board%d", link_);
#endif

  if (verbosity_) std::cout << GetName() << "::SetODBRecord(" << h << "," << set_str << ",...)" << std::endl;

  //create record if doesn't exist and find key
  status = db_create_record(h, 0, set_str, strcomb(config_str));
  status = db_find_key(h, 0, set_str, &settings_handle_);
  if (status != DB_SUCCESS) {
    cm_msg(MINFO,"FE","Key %s not found", set_str);
  }

  //hotlink
  size = sizeof(V1740_CONFIG_SETTINGS);
  status = db_open_record(h, settings_handle_, &config, size, MODE_READ, cb_func, NULL);
  if (status != DB_SUCCESS){
    cm_msg(MERROR,"SetBoardRecord","Couldn't create hotlink for %s. Return code: %d", set_str, status);
    return status;
  }

  //get actual record
  status = db_get_record(h, settings_handle_, &config, &size, 0);
  if (status != DB_SUCCESS){
    cm_msg(MERROR,"SetBoardRecord","Couldn't get record %s. Return code: %d", set_str, status);
    return status;
  }

  settings_loaded_ = true;

  return status; //== DB_SUCCESS for success
}

//
//---------------------------------------------------------------------------------------------------------
/**
 * \brief   Reset the board
 *
 *
 * \return  0 on success, -1 on error
 */
int v1740CONET2::ResetBoard() {

	CAENComm_ErrorCode sCAEN;
  // Clear the board                                                         
  sCAEN = WriteReg_(V1740_SW_RESET, 0x1);

  // Need time for the PLL to lock
  ss_sleep(200);

  return sCAEN;
}
//
//---------------------------------------------------------------------------------------------------------
/**
 * \brief   Initialize the frontend, check firmware version
 *
 *
 * \return  0 on success, -1 on error
 */
int v1740CONET2::InitializeForAcq() {
  if (!settings_loaded_) {
    cm_msg(MERROR,"InitializeForAcq"
           ,"Cannot call InitializeForAcq() without settings loaded properly on board %d\n"
           , this->GetModuleID());
    return -1;
  }
  if (!IsConnected()){
    cm_msg(MERROR,"InitializeForAcq","Board %d disconnected\n", this->GetModuleID());
    return -1;
  }
  if (IsRunning()){
    cm_msg(MERROR,"InitializeForAcq","Board %d already started\n", this->GetModuleID());
    return -1;
  }

  CAENComm_ErrorCode sCAEN;
  uint32_t temp;
               
	// Do board reset
	ResetBoard();

  // Clear done by accessing Buffer Origanization later on
  //  sCAEN = WriteReg_(V1740_SW_CLEAR, 0x1);
  
  //------------
  // Debugging cm_msg
  std::stringstream ss_fw_datatype;
  ss_fw_datatype << "Module " << moduleID_ << ", ";
  // Firmware version check (0x1n8C)
  // read each AMC firmware version
  // [31:16] Revision date Y/M/DD
  // [15:8] Firmware Revision (X)
  // [7:0] Firmware Revision (Y)
  // eg 0x760C0103 is 12th June 07, revision 1.3
  int addr = 0;
  uint32_t version = 0;
  uint32_t prev_chan = 0;
  // Hardcode correct firmware verisons
  // Current release 3.4_0.11 Feb 2012
  // AMC FW: 0xd3280005, ROC FW: 0xd4110401
  //  const uint32_t amc_fw_ver = 0xd3280005;
  //  const uint32_t roc_fw_ver = 0xd4110401;
  //  const uint32_t amc_fw_ver = 0xda210007;  // A3818-1.5.2
  //  const uint32_t roc_fw_ver = 0xe3110402;  // A3818-1.5.2
  //  const uint32_t amc_fw_ver = 0xe5270008;  // A3818-1.5.2
  //  const uint32_t roc_fw_ver = 0xe5270402;  // A3818-1.5.2
  const uint32_t amc_fw_ver = 0xea210008;  // Update: 22Oct2014
  const uint32_t roc_fw_ver = 0xe9100403;  // Update: 22Oct2014
//  const uint32_t amc_fw_ver = 0x0401000a;  // Update: 09September2016
//  const uint32_t roc_fw_ver = 0x0401040a;  // Update: 09September2016
  
  for(int iCh=0;iCh<8;iCh++) {
    addr = 0x108c | (iCh << 8);
    sCAEN = ReadReg_(addr, &version);
    if((iCh != 0) && (prev_chan != version)) {
      cm_msg(MERROR, "InitializeForAcq","Error Channels have different AMC Firmware ");
    }
    prev_chan = version;
  }
  if(version != amc_fw_ver)
    cm_msg(MERROR,"InitializeForAcq"
           ,"Incorrect AMC Firmware Version is 0x%08x/0x%08x", version, amc_fw_ver);
  else
    ss_fw_datatype << "AMC FW: 0x" << std::hex << version << ", ";
  
  // read ROC firmware revision
  // Format as above
  sCAEN = ReadReg_(V1740_ROC_FPGA_FW_REV, &version);
  switch (version)
    {
    case roc_fw_ver:
      ss_fw_datatype << "ROC FW: 0x" << std::hex << version << " ";
      break;
    default:
      cm_msg(MERROR,"InitializeForAcq"
             ,"Incorrect ROC Firmware Version is 0x%08x/0x%08x", version, roc_fw_ver);
      break;
    }
  
	cm_msg(MINFO, "StartRun", ss_fw_datatype.str().c_str());

  //ready to do start run
  return 0;
}

//
//---------------------------------------------------------------------------------------------------------
/**
 * \brief Check board type
 *
 * Check if board type is v1740, if not issue a warning
 */
void v1740CONET2::CheckBoardType() {
  // Verify Board Type
  uint32_t version = 0;
  const uint32_t v1740_board_type = 0x04;
  CAENComm_ErrorCode sCAEN = ReadReg_(V1740_BOARD_INFO, &version);
  printf("### version: 0x%08x\n", version);
  if((version & 0xFF) != v1740_board_type)
    cm_msg(MINFO,"feoV1740","*** WARNING *** Trying to use a v1740 frontend with another"
           " type of board.  Results will be unexpected!");

  UNUSED(sCAEN);
}

//
//---------------------------------------------------------------------------------------------------------
/**
 * \brief Misc reg printout, for debug purposes
 */
void v1740CONET2::ShowRegs() {
#if !SIMULATION
  DWORD temp;
  CAENComm_ErrorCode sCAEN;
  std::cout << "   *** regs: ";
  sCAEN = CAENComm_Read32(device_handle_, V1740_EVENT_SIZE, &temp); std::cout << "E_SIZE=0x" << std::hex <<  temp << " ";
  sCAEN = CAENComm_Read32(device_handle_, V1740_EVENT_STORED, &temp); std::cout << "E_STORED=0x" << std::hex <<  temp << " ";
  sCAEN = CAENComm_Read32(device_handle_, V1740_GROUP_BUFFER_OCCUPANCY, &temp); std::cout << "GRP_BUFF_OCC[0]=0x" << temp << " ";
  sCAEN = CAENComm_Read32(device_handle_, V1740_ACQUISITION_STATUS, &temp); std::cout << "ACQ_STATUS=0x" << temp << " lam=" << (temp & 0x4);
  //sCAEN = CAENComm_Read32(_device_handle, V1740_BLT_EVENT_NB, &temp);  << "BLT_EVENT_NB=0x" << temp << " ";
  //sCAEN = CAENComm_Read32(_device_handle, V1740_GROUP_CONFIG, &temp);  << "GROUP_CONFIG= " << temp << " ";
  //sCAEN = CAENComm_Read32(_device_handle, V1740_GROUP_EN_MASK, &temp);  << "GROUP_EN_MASK= " << temp << " ";
  std::cout << std::dec << std::endl;
  UNUSED(sCAEN);
#endif
}

//
//---------------------------------------------------------------------------------------------------------
CAENComm_ErrorCode v1740CONET2::GroupDACSet(int handle, uint32_t channel, uint32_t dac) {
  uint32_t reg, status, ncount;
  
  if ((channel >= 0) && (channel < 8)) {
    reg = V1740_GROUP_STATUS | (channel << 8);
    ncount = 10000;
    do {
      CAENComm_Read32(handle, reg, &status);
    } while ((status & 0x04) && (ncount--));
    if (ncount == 0) return (CAENComm_ErrorCode)-1;
    reg = V1740_GROUP_DAC | (channel << 8);
    return CAENComm_Write32(handle, reg, (dac & 0xFFFF));
  }
  return (CAENComm_ErrorCode)-1;
}

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 2
 * c-basic-offset: 2
 * End:
 */
