/*****************************************************************************/
/**
\file v1740CONET2.hxx

## Contents

This file contains the class definition for the v1740 module driver.
 *****************************************************************************/

#ifndef V1740_HXX_INCLUDE
#define V1740_HXX_INCLUDE

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <atomic>


#include <CAENComm.h>
#include <CAENVMElib.h>
#include "ov1740drv.h"

#include "midas.h"
#include "msystem.h"

/**
 * Driver class for the v1740 module using the CAEN CONET2 (optical) interface.
 * Contains all the methods necessary to:
 *
 * - Connect/disconnect the board through an optical connection
 * - Initialize the hardware (set the registers) for data acquisition
 * - Read and write to the ODB
 * - Poll the hardware and read the event buffer into a midas bank
 * - Send a software trigger to the board if desired
 */

class v1740CONET2
{
public:

  /* Enums/structs */
  struct V1740_CONFIG_SETTINGS {
    INT       setup; //!< Initial board setup mode number
    INT       acq_mode;               //!< 0x8100@[ 1.. 0]
    DWORD     group_config;           //!< 0x8000@[19.. 0]
    INT       buffer_organization;    //!< 0x800C@[ 3.. 0]
    INT       custom_size;            //!< 0x8020@[31.. 0]
    DWORD     group_mask;             //!< 0x8120@[ 7.. 0]
    DWORD     trigger_source;         //!< 0x810C@[31.. 0]
    DWORD     trigger_output;         //!< 0x8110@[31.. 0]
    DWORD     post_trigger;           //!< 0x8114@[31.. 0]
    DWORD     almost_full;            //!< 0x816C@[31.. 0]
    DWORD     threshold[8];           //!< 0x1n80@[11.. 0]
    DWORD     dac[8];                 //!< 0x1n98@[15.. 0]
    BOOL      enable_filtering;       //!< Whether to only pass through waveforms with "large" pulses
    INT       filtering_threshold;    //!< The threshold below which we'll send a waveform
  } config; //!< instance of config structure

  /* static */
  static const char *config_str[];

  /* Constructor/Destructor */
  v1740CONET2(int link, int board, int moduleID, HNDLE hDB);
  ~v1740CONET2();
  /* Use move instead of copy semantics as we only need one copy of each
   * object (C++11).  See notes in implementation. */
  v1740CONET2(v1740CONET2&&) noexcept;
  v1740CONET2& operator=(v1740CONET2&&) noexcept;

  /* Public methods */
  bool Connect();
  bool Disconnect();
  bool StartRun();
  bool StopRun();
  bool IsConnected();
  bool IsRunning();
  bool ReadReg(DWORD, DWORD*);
  bool WriteReg(DWORD, DWORD);
  bool CheckEvent();
  bool ReadEvent(void *);
  bool ReadEventCAEN(void *);
  bool FillEventBank(char *);
  bool FillBufferLevelBank(char *);
  bool SendTrigger();
  bool Poll(DWORD*);

	int ResetBoard();
  int SetBoardRecord(HNDLE h, void(*cb_func)(INT,INT,void*));
  int InitializeForAcq();
  void CheckBoardType();
  void ShowRegs();
  CAENComm_ErrorCode GroupDACSet(int, uint32_t, uint32_t);


  /* Getters/Setters */
  int GetModuleID() { return moduleID_; }       //!< returns unique module ID
  int GetLink() { return link_; }               //!< returns optical link number
  int GetBoard() { return board_; }             //!< returns board number
  std::string GetName();

  int GetDeviceHandle() {
    return device_handle_;                      //! returns physical device handle
  }
  HNDLE GetODBHandle() {                        //! returns main ODB handle
    return odb_handle_;
  }
  HNDLE GetSettingsHandle() {                   //! returns settings record handle
    return settings_handle_;
  }
  void SetRingBufferHandle(int rb_handle) {     //! set ring buffer index
    rb_handle_ = rb_handle;
  }
  int GetRingBufferHandle() {                   //! returns ring buffer index
    return rb_handle_;
  }
  int GetNumEventsInRB() {                      //! returns number of events in ring buffer
    return num_events_in_rb_.load();
  }
  int GetVerbosity(){
    return verbosity_;
  }
  void SetVerbosity(int verbosity){
    verbosity_ = verbosity;
  }

  /* These are atomic with sequential memory ordering. See below */
  void IncrementNumEventsInRB() {               //! Increment Number of events in ring buffer
    ++num_events_in_rb_;
  }
  void DecrementNumEventsInRB() {               //! Decrement Number of events in ring buffer
    --num_events_in_rb_;
  }
  void ResetNumEventsInRB() {                   //! Reset Number of events in ring buffer
    num_events_in_rb_=0;
  }


  /// Read raw data from src, and write filtered waveforms to dest. The
  /// filtering is based on whether the waveform drops below the filter threshold.
  /// Currently keeps the V1740 data structure of mixing 8-channel groups.
  ///
  /// Returns the number of words written to dest.
  int WriteFilteredWaveforms(DWORD* src, DWORD* dest);

private:
  int link_,      //!< Optical link number
  board_,         //!< Module/Board number
  moduleID_;      //!< Unique module ID

  int device_handle_;     //!< physical device handle
  HNDLE odb_handle_;      //!< main ODB handle
  HNDLE settings_handle_; //!< Handle for the device settings record
  int rb_handle_;         //!< Handle to ring buffer
  bool settings_loaded_;  //!< ODB settings loaded
  bool running_;          //!< Run in progress
  int verbosity_;   //!< Make the driver verbose
                    //!< 0: off
                    //!< 1: normal
                    //!< 2: very verbose
  /* We use an atomic types here to get lock-free (no pthread mutex lock or spinlock)
   * read-modify-write. operator++(int) and operator++() on an atomic<integral> use
   * atomic::fetch_add() and operator--(int) and operator--() use atomic::fetch_sub().
   * The default memory ordering for these functions is memory_order_seq_cst (sequentially
   * consistent). This saves us from inserting a memory barrier between read/write pointer
   * incrementation and an increment/decrement of this variable.   */
  std::atomic<int> num_events_in_rb_;  //!< Number of events stored in ring buffer

  /* Private methods */
  CAENComm_ErrorCode AcqCtl_(uint32_t);
  CAENComm_ErrorCode SetupPreset_(int);
  CAENComm_ErrorCode ReadReg_(DWORD, DWORD*);
  CAENComm_ErrorCode WriteReg_(DWORD, DWORD);
};


#endif // V1740_HXX_INCLUDE
