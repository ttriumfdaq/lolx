#include "TAnaManager.hxx"
#include "TRB3Decoder.hxx"

TAnaManager::TAnaManager(){

#ifdef USE_V792
  AddHistogram(new TV792Histograms());
#endif

#ifdef USE_V1190
  AddHistogram(new TV1190Histograms());
#endif
  
#ifdef USE_L2249
  AddHistogram(new TL2249Histograms());
#endif
  
#ifdef USE_AGILENT
  AddHistogram(new TAgilentHistograms());
#endif

#ifdef USE_V1740
    AddHistogram(new TV1740Waveform());
#endif

#ifdef USE_V1720
  AddHistogram(new TV1720Correlations());
#endif
  
#ifdef USE_V1730DPP
  AddHistogram(new TV1730DppWaveform());
#endif
  
#ifdef USE_V1730RAW
  AddHistogram(new TV1730RawWaveform());
#endif

#ifdef USE_DT724
  AddHistogram(new TDT724Waveform());
#endif
  
#ifdef USE_TRB3
  // Set the TDC linear calibration values
  Trb3Calib::getInstance().SetTRB3LinearCalibrationConstants(17,471);
  AddHistogram(new TTRB3Histograms());
  AddHistogram(new TTRB3FineHistograms());
  AddHistogram(new TTRB3DiffHistograms());
#endif
  
#ifdef USE_CAMACADC
  AddHistogram(new TCamacADCHistograms());
#endif
  

      
};


void TAnaManager::AddHistogram(THistogramArrayBase* histo) {
  histo->DisableAutoUpdate();

  //histo->CreateHistograms();
  fHistos.push_back(histo);

}


int TAnaManager::ProcessMidasEvent(TDataContainer& dataContainer){

  // Fill all the  histograms
  for (unsigned int i = 0; i < fHistos.size(); i++) {
    // Some histograms are very time-consuming to draw, so we can
    // delay their update until the canvas is actually drawn.
    if (!fHistos[i]->IsUpdateWhenPlotted()) {
      fHistos[i]->UpdateHistograms(dataContainer);
    }
  }
  

  return 1;
}


// Little trick; we only fill the transient histograms here (not cumulative), since we don't want
// to fill histograms for events that we are not displaying.
// It is pretty slow to fill histograms for each event.
void TAnaManager::UpdateTransientPlots(TDataContainer& dataContainer){
  std::vector<THistogramArrayBase*> histos = GetHistograms();
  
  for (unsigned int i = 0; i < histos.size(); i++) {
    if (histos[i]->IsUpdateWhenPlotted()) {
      histos[i]->UpdateHistograms(dataContainer);
    }
  }
}


