// Base class for writing analysis code using online software
#ifndef TV1740Handler_h
#define TV1740Handler_h

#include "TMidasEvent.h"
#include <vector>
#include "TH1.h"
using std::vector;

class TH1F;

//todo: don't hardcode constants

class TV1740Handler {

public:

  //stores event data for one v1740
  class EventData {
  protected:
    //always contains 64 histos
    //bool set to false if the waveform doesn't exist
    //this way we're not constantly re-initializing TH1F's
    vector<TH1F*> waveforms;
    vector<bool> exists;
    int event_number;
  public:
    static const int ChannelsPerGroup = 8;
    static const int GroupsPerBoard = 8;
    static const int ChannelsPerBoard = ChannelsPerGroup * GroupsPerBoard;
    EventData();
    ~EventData();

    TH1F* GetWaveform(int) const;
    void ProcessEvent(unsigned int  *, int, std::string bank_name="V1740");
    int GetEventNumber() const;
    bool IsEmpty() const;
  };

  static const int NumberOfBoards = 4;           
  static const int TotalChannels = NumberOfBoards * EventData::ChannelsPerBoard;
  TV1740Handler();               //constructor
  ~TV1740Handler();              //destructor
  void ProcessEvent(TMidasEvent&);  //main event handler
  EventData* GetEventData(int) const;             //get data for 1 board
  int GetEventNumber() const;
protected: 
  vector<EventData*> event_data;
};

#endif

