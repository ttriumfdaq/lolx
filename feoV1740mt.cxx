/****************************************************************************/
/**
	 \file feoV1740.cxx

	 \mainpage

	 \section contents Contents

	 Standard Midas Frontend for Optical access to the CAEN v1740 using the A3818 CONET2 driver

	 \subsection notes Notes about this frontend

	 This example has been designed so that it should compile and work by default
	 without actual actual access to v1740 hardware. We have turned off portions
	 of code which make use of the driver to the actual hardware.  Where data
	 acquisition should be performed, we generate random data instead
	 (see v1740CONET2::ReadEvent).  See usage below to use real hardware.

	 The setup assumed in this example is the following:
	 - 1 frontend
	 - 1 optical link per frontend
	 - Arbitrary number of v1740 modules connected to each optical link (daisy-chained)

	 For a more complicated setup (many modules per frontend, or many frontends
	 using the event building mechanism), see the v1720 example

	 \subsection usage Usage

	 \subsubsection simulation Simulation
	 Simply set the Nv1740 macro below to the number of boards you wish to simulate
	 and run "make SIMULATION=1".  Run ./feoV1740.exe

	 \subsubsection real Real hardware
	 Set the Nv1740 macro below to the number of boards that should be controlled by
	 the frontend on a specific optical link.  The frontend can be assigned an index
	 number (ie ./frontend -i 0), and this will be assumed to be the optical link
	 number to communicate with, after which all boards on that link will be started.
	 If no index number is assigned, the frontend will communicate with link 0 and
	 start all the boards on that link.

	 Compile using "make SIMULATION=0".  Run ./feoV1740.exe



	 \section deap DEAP-3600 notes

	 MIDAS_SERVER_HOST should be set to deap00:7071. Otherwise frontends will require the 
	 option -h deap00:7071

	 Each frontend will only access one link. To access all boards they
	 should be run four times on each computer with each of the four indexes 
	 \subsection deapusage Usage

	 - on deap05: ./bin/feoV1740.exe -i [ 0, 1, 2, 3 ] 
	 \subsection deapfiles Files

	 - feoV1740.cxx : Main front-end user code
	 - v1740CONET2.hxx / v1740CONET2.cxx : Driver class for the v1740 modules
	 using the CAEN CONET2 (optical) interface

	 $Id: feov1740.cxx 128 2011-05-12 06:26:34Z alex $
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <sched.h>
#include <sys/resource.h>

#include <vector>
using std::vector;

#include "midas.h"
#include "mfe.h"
#include "mvmestd.h"
#include "v1740CONET2.hxx"

#ifndef Nv1740
#define Nv1740    1 //!< Set the number of v1740 modules to be used
#endif

#define EQ_EVID   1 //!< Event ID

#define EQ_TRGMSK 0x20 //!< Trigger mask

#define FE_NAME   "feoV1740MT"    //!< Frontend name

#define UNUSED(x) ((void)(x))   //!< Suppress compiler warnings
//#define DEBUGTHREAD

/* Globals */
/* Hardware */
extern HNDLE hDB;   //!< main ODB handle
//extern BOOL debug;  //!< debug printouts

/* make frontend functions callable from the C framework */

	/*-- Globals -------------------------------------------------------*/

	//! The frontend name (client name) as seen by other MIDAS clients
	const char *frontend_name = (char*)FE_NAME;
	//! The frontend file name, don't change it
	const char *frontend_file_name = (char*)__FILE__;
	//! frontend_loop is called periodically if this variable is TRUE
	BOOL frontend_call_loop = FALSE;
	//! a frontend status page is displayed with this frequency in ms
	INT display_period = 000;
	//! maximum event size produced by this frontend
	//	INT max_event_size = 32 * 34000;    //in bytes
	INT max_event_size = 2000000;    //in bytes
	//! maximum event size for fragmented events (EQ_FRAGMENTED)
	INT max_event_size_frag = 5 * 1024 * 1024;
	//! buffer size to hold events
	//	INT event_buffer_size = 200 * max_event_size;
	INT event_buffer_size = 25 * max_event_size;

	bool runInProgress = false; //!< run is in progress

	/*-- Function declarations -----------------------------------------*/
	INT frontend_init();
	INT frontend_exit();
	INT begin_of_run(INT run_number, char *error);
	INT end_of_run(INT run_number, char *error);
	INT pause_run(INT run_number, char *error);
	INT resume_run(INT run_number, char *error);
	INT frontend_loop();
	extern void interrupt_routine(void);  //!< Interrupt Service Routine

	INT read_event_from_ring_bufs(char *pevent, INT off);
	INT read_buffer_level(char *pevent, INT off);
	void * link_thread(void *);

	/*-- Equipment list ------------------------------------------------*/
	//! Main structure for midas equipment
	EQUIPMENT equipment[] =
		{
			{
        "FEV1740MT",                  /* equipment name */
        {
					EQ_EVID, EQ_TRGMSK,       /* event ID, trigger mask */
					"BUF40",                  /* event buffer */
					EQ_POLLED| EQ_EB,         /* equipment type */

					0,       /* event source crate 0, all stations */
					"MIDAS",                  /* format */
					TRUE,                     /* enabled */
					RO_RUNNING,               /* read only when running */
					500,                      /* poll for 500ms */
					0,                        /* stop run after this event limit */
					0,                        /* number of sub events */
					0,                        /* don't log history */
					"", "", ""
        },
        read_event_from_ring_bufs,         /* readout routine */
			},

			{
        "BUFLVL40MT%02d",                /* equipment name */
        {
					104, 0,                   /* event ID, trigger mask */
					"SYSTEM",               /* event buffer */
					EQ_PERIODIC,   /* equipment type */
					0,                      /* event source */
					"MIDAS",                /* format */
					TRUE,                   /* enabled */
					RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
					RO_ODB,                 /* and update ODB */
					1000,                  /* read every 1 sec */
					0,                      /* stop run after this event limit */
					0,                      /* number of sub events */
					1,                      /* log history */
					"", "", ""
        },
        read_buffer_level,       /* readout routine */
			},

			{""}
		};



vector<v1740CONET2> ov1740; //!< objects for the v1740 modules controlled by this frontend
vector<v1740CONET2>::iterator itv1740; //!< iterator

pthread_t tid[Nv1740];                      //!< Thread ID
int thread_retval[Nv1740] = {0};            //!< Thread return value

/********************************************************************/
/********************************************************************/
/********************************************************************/

/**
 * \brief   Sequencer callback info
 *
 * Function which gets called when record is updated
 *
 * \param   [in]  hDB main ODB handle
 * \param   [in]  hseq Handle for key where search starts in ODB, zero for root.
 * \param   [in]  info Record descriptor additional info
 */
void seq_callback(INT hDB, INT hseq, void *info)
{
  KEY key;

  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (hseq == itv1740->GetSettingsHandle()) {
      db_get_key(hDB, hseq, &key);
      printf("Settings to %s touched. Changes will take effect at start of next run.\n", key.name);
    }
  }
}

//
//-------------------------------------------------------------------------
// check pll status; 1 indicates good PLL, 0 indicates PLL loss.
INT check_pll_status(){

	INT pll_status;
	pll_status = 1;

  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (! itv1740->IsConnected()) continue;   // Skip unconnected board
    DWORD vmeAcq, vmeStat;
    itv1740->ReadReg(V1740_ACQUISITION_STATUS, &vmeAcq); //Test the PLL lock once (it may have happened earlier)
    if ((vmeAcq & 0x80) == 0) { 
			cm_msg(MERROR,"BeginOfRun","V1740 PLL loss lock Board (sometime in the past):%d (vmeAcq=0x%x)"
						 ,itv1740->GetModuleID(), vmeAcq);
			// PLL loss lock reset by the VME_STATUS read!
			itv1740->ReadReg(V1740_VME_STATUS, &vmeStat);
			usleep(100);
			itv1740->ReadReg(V1740_ACQUISITION_STATUS, &vmeAcq); // Test the PLL again 
			if ((vmeAcq & 0x80) == 0) {
				cm_msg(MERROR,"BeginOfRun","V1740 PLL lock still lost Board: %d (vmeAcq=0x%x)"
							 ,itv1740->GetModuleID(), vmeAcq);
				pll_status = 0;
			}
    }
	}

	return pll_status;

}

//
//-------------------------------------------------------------------------
int tNActivev1740=0;  //Number of v1740 boards activated at the end of frontend_init
/**
 * \brief   Frontend initialization
 *
 * Runs once at application startup.  We initialize the hardware and optical
 * interfaces and set the equipment status in ODB.  We also lock the frontend
 *  to once physical cpu core.
 *
 * \return  Midas status code
 */
INT frontend_init()
{
  set_equipment_status(equipment[0].name, "Initializing...", "#FFFF00");
  printf("<<< Start of frontend_init\n");

  // Suppress watchdog for PCIe for now
  cm_set_watchdog_params(FALSE, 0);
  
  {
    // Reset the PLL lock loss flag in ODB
    char Path[255];
    sprintf(Path,"/DEAP Alarm/PLL Loss FE40");
    INT dummy;
    int size=sizeof(INT);
    db_get_value(hDB, 0, Path, &(dummy), &size, TID_INT, true);
    dummy=-1;
    db_set_value(hDB, 0, Path, &(dummy), sizeof(INT), 1, TID_INT);
  }

  // --- Get the frontend index. Derive the Optical link number
  int feIndex = get_frontend_index();
	int nInitOk = 0;
  for (int iBoard=0; iBoard < Nv1740; iBoard++) {
    printf("<<< Init board %i\n", iBoard);
                
    //If no index supplied, use board 0, else use index as board number
    //The link is always 0
    if(feIndex == -1) {
      ov1740.push_back(v1740CONET2(iBoard+1, 0, iBoard, hDB));
    } else {
      ov1740.push_back(v1740CONET2(iBoard, feIndex, iBoard, hDB));
    }
                
    // Open Optical interface
    printf("Opening optical interface Board %d\n", iBoard);
    if (ov1740.back().Connect()) {
      tNActivev1740++;
      ov1740.back().CheckBoardType();
    }

    ov1740.back().SetVerbosity(1);

  }

  /* This must be done _after_ filling the vector because we pass a pointer to config
   * to db_open_record.  The location of the object in memory must not change after
   * doing that. */
	for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
		if (! itv1740->IsConnected()) continue;   // Skip unconnected board

		// Setup ODB record (create if necessary)
		itv1740->SetBoardRecord(hDB,seq_callback);

		// Load HW with ODB record settings
		int status = itv1740->InitializeForAcq();
	
		nInitOk += status;
	}

	// Abort if board status not Ok.
	if (nInitOk != 0) return FE_ERR_HW;

	if(check_pll_status() == 0)
		return FE_ERR_HW;

  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(0, &mask);  //Main thread to core 0
  if( sched_setaffinity(0, sizeof(mask), &mask) < 0 ){
    printf("ERROR setting cpu affinity for main thread: %s\n", strerror(errno));
  }

	printf(">>> End of frontend_init. %d active v1740. Expected %d\n\n", tNActivev1740, Nv1740);
  set_equipment_status(equipment[0].name, "Initialized", "#00ff00");

#if SIMULATION
  printf("*** RUNNING SIMULATION ***\n");
#endif
  return SUCCESS;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Frontend exit
 *
 * Runs at frontend shutdown.  Disconnect hardware and set equipment status in ODB
 *
 * \return  Midas status code
 */
INT frontend_exit()
{
  set_equipment_status(equipment[0].name, "Exiting...", "#FFFF00");
  printf("<<< Begin of frontend_exit\n");

  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (itv1740->IsConnected()){
      itv1740->Disconnect();
    }
  }

  printf(">>> End of frontend_exit\n\n");
  set_equipment_status(equipment[0].name, "Exited", "#00ff00");
  return SUCCESS;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Begin of Run
 *
 * Called every run start transition.  Set equipment status in ODB,
 * start acquisition on the modules.
 *
 * \param   [in]  run_number Number of the run being started
 * \param   [out] error Can be used to write a message string to midas.log
 */
INT begin_of_run(INT run_number, char *error)
{

  set_equipment_status(equipment[0].name, "Starting run...", "#FFFF00");
  printf("<<< Begin of begin_of_run\n");
  int rb_handle;

  runInProgress = true;

  {
    // Reset the PLL lock loss flag in ODB
    char Path[255];
    sprintf(Path,"/DEAP Alarm/PLL Loss FE40");
    INT dummy;
		dummy=-1;
    db_set_value(hDB, 0, Path, &(dummy), sizeof(INT), 1, TID_INT);
	}

	// Reset every boards
  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (! itv1740->IsConnected()) continue;   // Skip unconnected board
		itv1740->ResetBoard();
	}

	// Check PLL loss
	if(check_pll_status() == 0)
		return FE_ERR_HW;


  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (! itv1740->IsConnected()) continue;   // Skip unconnected board

		//  ready ro start run; set hardware registers.
		// set start run ACQ mode
    bool status2 = itv1740->StartRun();
		if(status2 == false) 
			return FE_ERR_HW;

    //Create ring buffer for board
    int status = rb_create(event_buffer_size, max_event_size, &rb_handle);
    if(status == DB_SUCCESS) {
      itv1740->SetRingBufferHandle(rb_handle);
    } else {
      cm_msg(MERROR, "feoV1740MT:BOR", "Failed to create rb for board %d\n", itv1740->GetModuleID());
    }
		
    //Create one thread per optical link
    status = pthread_create(&tid[itv1740 - ov1740.begin()], NULL, &link_thread, (void*)&*itv1740);
    if(status) {
      cm_msg(MERROR,"feoV1740MT:BOR", "Couldn't create thread for board %d. Return code: %d\n",
						 itv1740->GetModuleID(), status);

    }
		else 
			cm_msg(MINFO,"BeginOfRun","Creation of Thread %d", itv1740->GetModuleID());
  }
	
  printf(">>> End of begin_of_run\n\n");
  set_equipment_status(equipment[0].name, "Started run", "#00ff00");
  return SUCCESS;
}

//
//-------------------------------------------------------------------------
void * link_thread(void * arg)
{
  v1740CONET2 * v1740 = (v1740CONET2 *) arg;

  std::cout << "Started thread for board " << v1740->GetModuleID() << std::endl;

  int link = *(int*)arg;
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(( v1740->GetModuleID() % 2) + 1, &mask);
  if( sched_setaffinity(0, sizeof(mask), &mask) < 0 ){
    printf("ERROR setting cpu affinity for thread %d: %s\n", link, strerror(errno));
  }

  void *wp;
  int status;
  int rb_handle = v1740->GetRingBufferHandle();
  int moduleID = v1740->GetModuleID();
  int rb_level;

  while(1) {

		/* If we've reached 75% of the ring buffer space, don't read
		 * the next event.  Wait until the ring buffer level goes down.
		 * It is better to let the v1740 buffer fill up instead of
		 * the ring buffer, as it will generate the HW busy to the DTM.
		 */
		rb_get_buffer_level(rb_handle, &rb_level);
		if(rb_level > (int)(event_buffer_size*0.75)) {
			usleep(100);
			continue;
		}               

		status = rb_get_wp(rb_handle, &wp, 100);
		if (status == DB_TIMEOUT) {
			cm_msg(MERROR,"link_thread", "Got wp timeout for link %d.  Is the ring buffer full?\n", moduleID);
			cm_msg(MERROR,"link_thread", "Exiting thread %d", moduleID);
			thread_retval[moduleID] = -1;
			pthread_exit((void*)&thread_retval[moduleID]);
		}
                
#ifdef DEBUGTHREAD
		printf("THREAD %d: Found event\n", moduleID);
#endif
                
		if (v1740->ReadEventCAEN(wp)) {
                        
#ifdef DEBUGTHREAD
			printf("THREAD %d: Successfully read event\n", moduleID);
			printf("THREAD %d: Events in buffer: %d\n", moduleID, v1740->GetNumEventsInRB());
                        
			printf("THREAD %d: Events in buffer 0: %d\n", moduleID, ov1740[0].GetNumEventsInRB());
			printf("THREAD %d: Events in buffer 1: %d\n", moduleID, ov1740[1].GetNumEventsInRB());
			printf("THREAD %d: Events in buffer 2: %d\n", moduleID, ov1740[2].GetNumEventsInRB());
			printf("THREAD %d: Events in buffer 3: %d\n", moduleID, ov1740[3].GetNumEventsInRB());
#endif
                        
		} else {
			cm_msg(MERROR,"link_thread", "Readout routine error %d on board %d\n", status, moduleID);
			cm_msg(MERROR,"link_thread", "Exiting thread %d", moduleID);
			thread_retval[moduleID] = -1;
			pthread_exit((void*)&thread_retval[moduleID]);
		}
                
		if(!runInProgress)
			break;
                
		//Sleep for 100 microsec to avoid hammering the board too much
	usleep(10);
	}
        
  printf("Exiting thread for board %d\n", v1740->GetModuleID());
  thread_retval[moduleID] = 0;
  pthread_exit((void*)&thread_retval[moduleID]);
}

//
//-------------------------------------------------------------------------
/**
 * \brief   End of Run
 *
 * Called every stop run transition. Set equipment status in ODB,
 * stop acquisition on the modules.
 *
 * \param   [in]  run_number Number of the run being ended
 * \param   [out] error Can be used to write a message string to midas.log
 */
INT end_of_run(INT run_number, char *error)
{
  set_equipment_status(equipment[0].name, "Ending run...", "#FFFF00");
  printf("\n<<< Begin of end_of_run \n");

  DWORD eStored;
  bool result;
  int * status;

  if(runInProgress){  //skip actions if we weren't running

    runInProgress = false;  //Signal threads to quit

    // Stop run
    for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
      if (! itv1740->IsConnected()) continue;   // Skip unconnected board
      result = itv1740->StopRun();

      if(!result)
        cm_msg(MERROR, "feoV1740MT:EOR", "Could not stop the run for module %d\n", itv1740->GetModuleID());

      pthread_join(tid[itv1740->GetModuleID()],(void**)&status);
      printf(">>> Thread %d joined, return code: %d\n", itv1740->GetModuleID(), *status);

      rb_delete(itv1740->GetRingBufferHandle());
      itv1740->SetRingBufferHandle(-1);
	  itv1740->ResetNumEventsInRB();
    }


#if SIMULATION
    result = ov1740[0].Poll(&eStored);
#else
    result = ov1740[0].Poll(&eStored);
    if(eStored != 0x0) {
      cm_msg(MERROR, "feoV1740MT:EOR", "Events left in the v1740: %d",eStored);
    }
#endif

  }

  printf(">>> End Of end_of_run\n\n");
  set_equipment_status(equipment[0].name, "Ended run", "#00ff00");

  return SUCCESS;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Pause Run
 *
 * Called every pause run transition.
 *
 * \param   [in]  run_number Number of the run being ended
 * \param   [out] error Can be used to write a message string to midas.log
 *
 * \return  Midas status code
 */
INT pause_run(INT run_number, char *error)
{
  printf("<<< Begin of pause_run \n");

  bool result;
  int * status;

  if(runInProgress){  //skip actions if we weren't running

    runInProgress = false;  //Signal threads to quit

    // Stop run
    for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
      if (! itv1740->IsConnected()) continue;   // Skip unconnected board
      result = itv1740->StopRun();

      if(!result)
        cm_msg(MERROR, "feoV1740MT:EOR", "Could not stop the run for module %d\n", itv1740->GetModuleID());

      pthread_join(tid[itv1740->GetModuleID()],(void**)&status);
      printf(">>> Thread %d joined, return code: %d\n", itv1740->GetModuleID(), *status);

      rb_delete(itv1740->GetRingBufferHandle());
      itv1740->SetRingBufferHandle(-1);
	  itv1740->ResetNumEventsInRB();
    }
  }

  printf(">>> End Of pause_run\n\n");
  return SUCCESS;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Resume Run
 *
 * Called every resume run transition.
 *
 * \param   [in]  run_number Number of the run being ended
 * \param   [out] error Can be used to write a message string to midas.log
 *
 * \return  Midas status code
 */
INT resume_run(INT run_number, char *error)
{
  printf("<<< Begin of resume_run \n");

  int rb_handle;

  runInProgress = true;

  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (! itv1740->IsConnected()) continue;   // Skip unconnected board

    itv1740->StartRun();

    //Create ring buffer for board
    int status = rb_create(event_buffer_size, max_event_size, &rb_handle);
    if(status == DB_SUCCESS){
      itv1740->SetRingBufferHandle(rb_handle);
    }
    else{
      cm_msg(MERROR, "feoV1740MT:BOR", "Failed to create rb for board %d\n", itv1740->GetModuleID());
    }

    //Create one thread per optical link
    status = pthread_create(&tid[itv1740 - ov1740.begin()], NULL, &link_thread, (void*)&*itv1740);
    if(status){
      cm_msg(MERROR,"feoV1740MT:BOR", "Couldn't create thread for board %d. Return code: %d\n",
						 itv1740->GetModuleID(), status);
    }
  }

  printf(">>> End Of resume_run\n\n");
  return SUCCESS;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Frontend loop
 *
 * If frontend_call_loop is true, this routine gets called when
 * the frontend is idle or once between every event.
 *
 * \return  Midas status code
 */
INT frontend_loop()
{
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\
  Readout routines for different events
\********************************************************************/
int Nloop;  //!< Number of loops executed in event polling
int Ncount; //!< Loop count for event polling timeout
DWORD acqStat; //!< ACQUISITION STATUS reg, must be global because read by poll_event, accessed by read_trigger_event

// ___________________________________________________________________
/*-- Trigger event routines ----------------------------------------*/
/**
 * \brief   Polling routine for events.
 *
 * \param   [in]  source Event source (LAM/IRQ)
 * \param   [in]  count Loop count for event polling timeout
 * \param   [in]  test flag used to time the polling
 * \return  1 if event is available, 0 if done polling (no event).
 * If test equals TRUE, don't return.
 */
 INT poll_event(INT source, INT count, BOOL test)
{
  register int i;

  for (i = 0; i < count; i++) {

    //ready for readout only when data is present in all ring buffers
    bool evtReady = true;
    for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
      if(itv1740->IsConnected() && (itv1740->GetNumEventsInRB() == 0)){
        evtReady = false;
      }
    }

    if (evtReady && !test)
      return 1;

  }
  return 0;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Interrupt configuration (not implemented)
 *
 * Routine for interrupt configuration if equipment is set in EQ_INTERRUPT
 * mode.  Not implemented right now, returns SUCCESS.
 *
 * \param   [in]  cmd Command for interrupt events (see midas.h)
 * \param   [in]  source Equipment index number
 * \param   [in]  adr Interrupt routine (see mfe.c)
 *
 * \return  Midas status code
 */
 INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

//
//-------------------------------------------------------------------------
/**
 * \brief   Trigger event readout
 *
 * Main trigger event readout routine.  This is called by the polling or interrupt routines.
 * (see mfe.c).  For each module, read the event buffer into a midas data bank.
 *
 * \param   [in]  pevent Pointer to event buffer
 * \param   [in]  off Caller info (unused here), see mfe.c
 *
 * \return  Size of the event
 */
INT read_event_from_ring_bufs(char *pevent, INT off) {
  if (!runInProgress) return 0;

  DWORD sn = SERIAL_NUMBER(pevent);
  bk_init32(pevent);

  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    if (!itv1740->IsConnected()) continue;   // Skip unconnected board
		//		printf("fragment:%d S/N:%d \n", itv1740->GetModuleID(), sn);
    itv1740->FillEventBank(pevent);
  }

  //primitive progress bar
  if (sn % 100 == 0) printf(".");

  return bk_size(pevent);
}

//                                                                                         
//-------------------------------------------------------------------------                
INT read_buffer_level(char *pevent, INT off) {
	
  bk_init32(pevent);
	
  int PLLLockLossID=-1;
  for (itv1740 = ov1740.begin(); itv1740 != ov1740.end(); ++itv1740) {
    itv1740->FillBufferLevelBank(pevent);
    DWORD vmeStat, vmeAcq;
    // Check the PLL lock                                                                  
    itv1740->ReadReg(V1740_ACQUISITION_STATUS, &vmeAcq);
    if ((vmeAcq & 0x80) == 0) {
      PLLLockLossID= itv1740->GetModuleID();
      cm_msg(MINFO,"read_buffer_level","V1740 PLL loss lock Board:%d (vmeAcq=0x%x)"
             , itv1740->GetModuleID(), vmeAcq);
      // PLL loss lock reset by the VME_STATUS read!                                       
      itv1740->ReadReg(V1740_VME_STATUS, &vmeStat);
    }
  }

  // Set ODB flag if PLL lock lost                                                         
  if (PLLLockLossID > -1) {
    char Path[255];
    sprintf(Path,"/DEAP Alarm/PLL Loss FE40");
    db_set_value(hDB, 0, Path, &(PLLLockLossID), sizeof(INT), 1, TID_INT);
  }

  printf(" | ");
  return bk_size(pevent);
}

/* emacs                                                                                   
 * Local Variables:                                                                        
 * mode:C                                                                                  
 * mode:font-lock                                                                          
 * tab-width: 2                                                                            
 * c-basic-offset: 2                                                                       
 * End:                                                                                    
 */
